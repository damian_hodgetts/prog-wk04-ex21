﻿using System;

namespace prog_wk04_ex21
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 5;
            
            for (var i = 0; i < counter; i++)
            {
                var a = i + 1;
                Console.WriteLine($"This is line number {a}");
            }
            var i2 = 0;
            var counter2 = 5;
            while (i2 < counter2)
            {
                var a = i2 + 1;
                Console.WriteLine($"This is line is number {a}");

                i2++;
            }
            var counter3 = 11;
            for (var i3 = 0; i3 < counter3; i3++)
            {
                var a = i3 * 2;
                Console.WriteLine($"This is Line Number {a}");
            }
            var counter4 = 11;
            var i4 = 0;
            while (i4 < counter4)
            {
                var a = i4 * 2;
                Console.WriteLine($"This is Line number {a}");

                i4++;
            }



        }
    }
}
